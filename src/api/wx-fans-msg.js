import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/fans/msg/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/fans/msg/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/fans/msg/update',
    method: 'post',
    data
  })
}

export function updateResContent(data) {
  return request({
    url: '/api/wx/fans/msg/updateResContent',
    method: 'post',
    data
  })
}

export function deleteRow(data) {
  return request({
    url: '/api/wx/fans/msg/delete',
    method: 'post',
    params:data
  })
}
