/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/Layout'

const sysUserManage = {
  path: '/sysUserManage',
  component: Layout,
  alwaysShow: true,
  name: '系统管理',
  redirect: 'noredirect',
  meta: {
    title: '系统管理',
    icon: 'component',
    resources: 'sysUserManage'
  },
  children: [
    {
      path: 'userList',
      component: () => import('@/views/system-web/system-manage/user-manage/userList'),
      name: '用户管理',
      meta: { title: '用户管理', icon: 'my-user', noCache: true, resources: 'userList' }
    },
    {
      path: 'roleList',
      component: () => import('@/views/system-web/system-manage/role-manage/roleList'),
      name: '角色管理',
      meta: { title: '角色管理', icon: 'my-role', noCache: true, resources: 'roleList' }
    },
    {
      path: 'roleSetting/:id(\\d+)',
      component: () => import('@/views/system-web/system-manage/role-manage/roleSetting'),
      name: '权限设置',
      meta: { title: '权限设置', noCache: true, resources: 'roleSetting' },
      hidden: true
    },
    {
      path: 'sysmenuList',
      component: () => import('@/views/system-web/system-manage/sysmenu-manage/sysmenuList'),
      name: 'sysmenuList',
      meta: { title: '菜单管理', icon: 'my-sysmenu', noCache: true, resources: 'sysmenuList' }
    }
  ]
}

export default sysUserManage
